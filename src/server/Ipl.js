function getmatchWon(matches) {
  return matches
    .map((mapValue) => {
      return {
        tossWinningTeam: mapValue.toss_winner,
        winningTeam: mapValue.winner,
      };
    })
    .reduce(
      (result, match) => {
        if (match.tossWinningTeam === match.winningTeam) {
          if (result[match.winningTeam]) {
            result[match.winningTeam] += 1;
          } else {
            result[match.winningTeam] = 1;
          }
        }
        return result;
      },
      {}
    );
}

function playerOfTheMatchPerSeason(matches) {
  var getCountOfPomPerSeason = matches
    .map((value) => {
      return { season: value.season, playerOfMatch: value.player_of_match };
    })
    .reduce((dataOfcount, match) => {
      if (dataOfcount[match.season]) {
        if (dataOfcount[match.season][match.playerOfMatch]) {
          dataOfcount[match.season][match.playerOfMatch] += 1;
        } else {
          dataOfcount[match.season][match.playerOfMatch] = 1;
        }
      } else {
        dataOfcount[match.season] = {};
        dataOfcount[match.season][match.playerOfMatch] = 1;
      }
      return dataOfcount;
    }, {});
  getCountOfPomPerSeason = Object.entries(getCountOfPomPerSeason);
  return getCountOfPomPerSeason.reduce((result, season) => {
    let maxTimes = Object.entries(season[1]).sort((a, b) => b[1] - a[1])[0];
    result[season[0]] = maxTimes;
    return result;
  }, {});
}

function getStrikeRateOfBatsman(matches, deliveries, playerName) {
  var playerData = deliveries.reduce((playerData, delivery) => {
    var id = delivery.match_id;
    var batsman = delivery.batsman;
    if (batsman == playerName) {
      var runs = parseInt(delivery.batsman_runs);
      if (playerData[id]) {
        playerData[id]["runs"] += runs;
        playerData[id]["balls"] += 1;
      } else {
        playerData[id] = {};
        playerData[id]["runs"] = runs;
        playerData[id]["balls"] = 1;
      }
    }
    return playerData;
  }, {});
  var playerDataPerSeason = matches.reduce((playerDataPerSeason, match) => {
    if (playerData[match.id]) {
      var season = [match.season];
      if (playerDataPerSeason[season]) {
        playerDataPerSeason[season]["runs"] += playerData[match.id]["runs"];
        playerDataPerSeason[season]["balls"] += playerData[match.id]["balls"];
      } else {
        playerDataPerSeason[season] = {};

        playerDataPerSeason[season]["runs"] = playerData[match.id]["runs"];
        playerDataPerSeason[season]["balls"] = playerData[match.id]["balls"];
      }
    }
    return playerDataPerSeason;
  }, {});
  playerDataPerSeason = Object.entries(playerDataPerSeason);
  var result = playerDataPerSeason.reduce((result, playerDataPerSeason) => {
    result[playerDataPerSeason[0]] =
      (playerDataPerSeason[1].runs / playerDataPerSeason[1].balls) * 100;

    return result;
  }, {});

  return result;
}

function highestdismissedPlayer(deliveries) {
  var dataOfPlayerDismissed = deliveries
    .map((value) => {
      return { playerDismissed: value.player_dismissed, bowler: value.bowler };
    })
    .reduce((dataOfPlayerDismissed, match) => {
      if (match.playerDismissed) {
        if (dataOfPlayerDismissed[match.playerDismissed]) {
          if (dataOfPlayerDismissed[match.playerDismissed][match.bowler]) {
            dataOfPlayerDismissed[match.playerDismissed][match.bowler] += 1;
          } else {
            dataOfPlayerDismissed[match.playerDismissed][match.bowler] = 1;
          }
        } else {
          dataOfPlayerDismissed[match.playerDismissed] = {};
          dataOfPlayerDismissed[match.playerDismissed][match.bowler] = 1;
        }
      }
      return dataOfPlayerDismissed;
    }, {});
  dataOfPlayerDismissed = Object.entries(dataOfPlayerDismissed);
  var resultOfSort = dataOfPlayerDismissed.reduce(
    (resultOfSort, dataOfPlayerDismissed) => {
      var max = Object.entries(dataOfPlayerDismissed[1]).sort(
        (a, b) => b[1] - a[1]
      )[0];
      resultOfSort[dataOfPlayerDismissed[0]] = max;
      return resultOfSort;
    },
    {}
  );
  result = Object.entries(resultOfSort);
  let max = result.sort((a, b) => b[1][1] - a[1][1])[0];
  return {
    batsman: max[0],
    "dismissed count": max[1][1],
    bowler: max[1][0],
  };
}

function bestEconomicBowler(matches, deliveries) {
  var playerData = deliveries
    .map((value) => {
      return {
        isSuperOver: value.is_super_over,
        bowler: value.bowler,
        totalRuns: value.total_runs,
      };
    })
    .reduce((result, delivery) => {
      var isSuperOver = parseInt(delivery.isSuperOver);
      if (isSuperOver == 1) {
        var totalRuns = parseInt(delivery.totalRuns);
        var bowler = delivery.bowler;

        if (result[bowler]) {
          result[bowler]["runs"] += totalRuns;
          result[bowler]["balls"] += 1;
        } else {
          result[bowler] = {};
          result[bowler]["runs"] = totalRuns;
          result[bowler]["balls"] = 1;
        }
      }

      return result;
    }, {});
  playerData = Object.entries(playerData);
  var economicRate = playerData.reduce((economicRate, playerData) => {
    economicRate[playerData[0]] =
      (playerData[1].runs * 6) / playerData[1].balls;
    return economicRate;
  }, {});
  var economicRate = Object.entries(economicRate);
  var result = economicRate.sort((a, b) => a[1] - b[1])[0];
  return result;
}

module.exports = {
  getmatchWon: getmatchWon,
  playerOfTheMatchPerSeason: playerOfTheMatchPerSeason,
  getStrikeRateOfBatsman: getStrikeRateOfBatsman,
  highestdismissedPlayer: highestdismissedPlayer,
  bestEconomicBowler: bestEconomicBowler,
};
