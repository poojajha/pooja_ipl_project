const fs = require("fs");
const path = require("path");
const csv = require("csvtojson");
const matchesFilePath = path.join(__dirname, "../data/matches.csv");
const deliveriesFilePath = path.join(__dirname, "../data/deliveries.csv");
const outputFilePath1 = path.join(
  __dirname,
  "..",
  "public",
  "output",
  "Alldata.json"
);
const ipl = require("./Ipl.js");

function main() {
  csv()
    .fromFile(matchesFilePath)
    .then((matches) => {
      csv()
        .fromFile(deliveriesFilePath)
        .then((deliveries) => {
          var getMatchWonResult = ipl.getmatchWon(matches);
          var playerOfTheMatchPerSeasonResult = ipl.playerOfTheMatchPerSeason(matches, deliveries);
          var getStrikeRateOfBatsmanResult = ipl.getStrikeRateOfBatsman(matches, deliveries,"MS Dhoni");
          var highestdismissedPlayerResult = ipl.highestdismissedPlayer(deliveries);
          var bestEconomicBowlerResult= ipl.bestEconomicBowler(matches, deliveries);
          const jsonData = {
            getmatchWon:getMatchWonResult ,
            playerOfTheMatchPerSeason :playerOfTheMatchPerSeasonResult ,
            getStrikeRateOfBatsman: getStrikeRateOfBatsmanResult,
            highestdismissedPlayer: highestdismissedPlayerResult,
            bestEconomicBowler: bestEconomicBowlerResult,
          };
          saveResult(jsonData);
        });
    });
}
main();
function saveResult(jsonData) {
  var jsonContent = JSON.stringify(jsonData);

  fs.writeFile(outputFilePath1, jsonContent, "utf-8", function (err) {
    if (err) {
      console.log("An error occured while writing JSON Object to File.");
      return console.log(err);
    }
  });
}

